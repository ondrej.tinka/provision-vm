provider "digitalocean" {
    token = var.do_token
}

variable "do_token" {
  type = string
}

variable "ssh_publickey_path" {
  type = string
}

resource "digitalocean_ssh_key" "Macbook-temporary" {
  name       = "Macbook-temporary"
  public_key = file("${var.ssh_publickey_path}/id_rsa_Macbook_temporary.pub")
}

resource "digitalocean_ssh_key" "ansible" {
  name       = "ansible"
  public_key = file("${var.ssh_publickey_path}/id_rsa_ansible.pub")
}

resource "digitalocean_ssh_key" "jenkins" {
  name       = "jenkins"
  public_key = file("${var.ssh_publickey_path}/id_rsa_jenkins.pub")
}

resource "digitalocean_droplet" "dvmsa01" {
    name  = "dvmsa01"
    image = "centos-7-x64"
    region = "fra1"
    size   = "s-1vcpu-1gb"
    private_networking = true
    ssh_keys = [digitalocean_ssh_key.Macbook-temporary.fingerprint, digitalocean_ssh_key.ansible.fingerprint, digitalocean_ssh_key.jenkins.fingerprint]
}


output "ip" {
    value = "${digitalocean_droplet.dvmsa01.ipv4_address}"
}
